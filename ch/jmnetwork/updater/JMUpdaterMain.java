package ch.jmnetwork.updater;

import ch.jmnetwork.updater.ui.InputGUI;
import ch.jmnetwork.updater.ui.JMUpdater;
import ch.jmnetwork.vapi.VersionApi;

public class JMUpdaterMain {

    private static String PROJECT_URL;
    private static String INSTALLED_VERSION;
    private static String UPDATE_TO;
    private static boolean AUTOCLOSE_AFTER;
    private static String INSTALL_DIR;
    private static VersionApi vapi; // = new VersionApi("updatevconf.xml");
    static JMUpdater jud;

    public static void main(String[] args) {
        if (args.length == 5) {
            vapi = new VersionApi("updatevconf.xml");

            if (args[0].equals("jmath")) {
                PROJECT_URL = "http://www.jmnetwork.ch/public/jm-software/jmath/";
            } else if (args[0].equals("cookieclicker")) {
                PROJECT_URL = "http://www.jmnetwork.ch/public/jm-software/cookieclicker/";
            } else if (args[0].equals("cccalc")) {
                PROJECT_URL = "http://www.jmnetwork.ch/public/jm-software/cookieclickercalc/";
            } else {
                PROJECT_URL = args[0];
            }

            INSTALLED_VERSION = args[1];
            UPDATE_TO = args[2];
            INSTALL_DIR = args[3];
            AUTOCLOSE_AFTER = Boolean.parseBoolean(args[4]);

            vapi.setVersioningURL(PROJECT_URL);
            vapi.reload();

            if (UPDATE_TO.toUpperCase().contains("NEWEST")) {
                jud = new JMUpdater(PROJECT_URL, INSTALLED_VERSION, UPDATE_TO + " (" + vapi.getNewestVersion() + ")", AUTOCLOSE_AFTER, vapi, INSTALL_DIR);
            } else {
                jud = new JMUpdater(PROJECT_URL, INSTALLED_VERSION, UPDATE_TO, AUTOCLOSE_AFTER, vapi, INSTALL_DIR);
            }
        } else {
            System.out.println("USAGE: JMUpdater.jar {PROJECT_URL (VersionAPI)} {INSTALLED_VERSION} {UPDATE_TO_VERSION (Version or NEWEST)} {DIRECTORY_TO_INSTALL} {AUTOCLOSE_AFTER_UPDATE} ");
            new InputGUI();
        }
    }
}
