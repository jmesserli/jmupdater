package ch.jmnetwork.updater.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ch.jmnetwork.updater.JMUpdaterMain;


public class InputGUI {

	private JFrame frmJmupdaterGui;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// InputGUI window = new InputGUI();
	// window.frmJmupdaterGui.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	public InputGUI() {

		initialize();
		frmJmupdaterGui.setVisible(true);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() {

		frmJmupdaterGui = new JFrame();
		frmJmupdaterGui.setTitle("JMUpdater GUI\r\n");
		frmJmupdaterGui.setBounds(100, 100, 539, 228);
		frmJmupdaterGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmJmupdaterGui.getContentPane().setLayout(null);

		JLabel lblVersionapiUrl = new JLabel("VersionAPI URL:");
		lblVersionapiUrl.setBounds(10, 11, 503, 14);
		frmJmupdaterGui.getContentPane().add(lblVersionapiUrl);

		final JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "", "jmath", "cookieclicker", "cccalc" }));
		comboBox.setEditable(true);
		comboBox.setBounds(10, 36, 503, 20);
		frmJmupdaterGui.getContentPane().add(comboBox);

		JLabel lblInstalledVersion = new JLabel("Installed version:");
		lblInstalledVersion.setBounds(10, 67, 243, 14);
		frmJmupdaterGui.getContentPane().add(lblInstalledVersion);

		textField = new JTextField();
		textField.setBounds(10, 92, 243, 20);
		frmJmupdaterGui.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblToVersion = new JLabel("To version:");
		lblToVersion.setBounds(277, 67, 236, 14);
		frmJmupdaterGui.getContentPane().add(lblToVersion);

		textField_1 = new JTextField();
		textField_1.setBounds(277, 92, 236, 20);
		frmJmupdaterGui.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		JLabel lblInstallDirectory = new JLabel("Install directory:");
		lblInstallDirectory.setBounds(10, 123, 243, 14);
		frmJmupdaterGui.getContentPane().add(lblInstallDirectory);

		textField_2 = new JTextField();
		textField_2.setBounds(10, 148, 243, 20);
		frmJmupdaterGui.getContentPane().add(textField_2);
		textField_2.setColumns(10);

		final JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] { "false", "true" }));
		comboBox_1.setBounds(277, 148, 74, 20);
		frmJmupdaterGui.getContentPane().add(comboBox_1);

		JLabel lblAutocloseAfterUpdate = new JLabel("Autoclose after update:");
		lblAutocloseAfterUpdate.setBounds(277, 123, 236, 14);
		frmJmupdaterGui.getContentPane().add(lblAutocloseAfterUpdate);

		JButton btnDownloadUpdate = new JButton("Download update");
		btnDownloadUpdate.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				String arguments[] = new String[5];

				arguments[0] = comboBox.getSelectedItem().toString();
				arguments[1] = textField.getText();
				arguments[2] = textField_1.getText().toUpperCase();
				arguments[3] = textField_2.getText();
				arguments[4] = comboBox_1.getSelectedItem().toString();

				JMUpdaterMain.main(arguments);

				frmJmupdaterGui.setVisible(false);
			}
		});
		btnDownloadUpdate.setBounds(374, 147, 139, 23);
		frmJmupdaterGui.getContentPane().add(btnDownloadUpdate);
	}
}
