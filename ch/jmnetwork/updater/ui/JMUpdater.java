package ch.jmnetwork.updater.ui;

import ch.jmnetwork.vapi.VersionApi;
import ch.jmnetwork.vapi.events.VapiListener;

import javax.swing.*;
import java.awt.*;


public class JMUpdater implements VapiListener {

    private JFrame frmJmupdater;
    private static String PROJECT_URL;
    private static String INSTALLED_VERSION;
    private static String UPDATE_TO;
    private static boolean AUTOCLOSE_AFTER;
    JProgressBar progressBar;
    JLabel lblDownloadingAndInstalling;
    JLabel lblVersion;
    JLabel lblMb;
    private int progress = 0;
    private long myContentLength = 0;

    public JMUpdater(String projecturl, String installedversion, String updatetoversion, boolean autocloseafter, VersionApi vapi, String INSTALL_DIR) {

        PROJECT_URL = projecturl;
        INSTALLED_VERSION = installedversion;
        UPDATE_TO = updatetoversion;
        AUTOCLOSE_AFTER = autocloseafter;

        vapi.addListener(this);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                initialize();
                frmJmupdater.setVisible(true);
            }
        });

        vapi.downloadNewVersion(UPDATE_TO.contains("NEWEST") ? vapi.getNewestVersion() : UPDATE_TO, INSTALL_DIR);
    }

    private void initialize() {

        frmJmupdater = new JFrame();
        frmJmupdater.setTitle("JMUpdater");
        frmJmupdater.setBounds(100, 100, 450, 144);
        frmJmupdater.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmJmupdater.setLocationRelativeTo(null);
        frmJmupdater.getContentPane().setLayout(null);
        frmJmupdater.setResizable(false);

        progressBar = new JProgressBar();
        progressBar.setForeground(Color.MAGENTA);
        progressBar.setIndeterminate(true);
        progressBar.setBounds(10, 11, 424, 5);
        frmJmupdater.getContentPane().add(progressBar);

        lblDownloadingAndInstalling = new JLabel("Downloading and installing update from:");
        lblDownloadingAndInstalling.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 12));
        lblDownloadingAndInstalling.setBounds(10, 37, 243, 14);
        frmJmupdater.getContentPane().add(lblDownloadingAndInstalling);

        lblVersion = new JLabel("Version " + INSTALLED_VERSION + " -> " + UPDATE_TO);
        lblVersion.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 12));
        lblVersion.setBounds(10, 91, 414, 14);
        frmJmupdater.getContentPane().add(lblVersion);

        JLabel label = new JLabel(PROJECT_URL);
        label.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 10));
        label.setBounds(10, 52, 424, 14);
        frmJmupdater.getContentPane().add(label);

        lblMb = new JLabel("N/A MB");
        lblMb.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 12));
        lblMb.setHorizontalAlignment(SwingConstants.RIGHT);
        lblMb.setBounds(265, 35, 169, 16);
        frmJmupdater.getContentPane().add(lblMb);
    }

    public void setContentLength(Long downloaded, Long contentlength) {

        myContentLength = contentlength;

        float myContentLength2 = myContentLength;
        float myDownloaded = downloaded;

        myContentLength2 /= 1024F;
        myDownloaded /= 1024F;

        if (myContentLength2 <= 1024) {

            myContentLength2 = onlyTwoAfterComma(myContentLength2);
            myDownloaded = onlyTwoAfterComma(myDownloaded);
            lblMb.setText(myDownloaded + " / " + myContentLength2 + " KB");
        } else if (myContentLength2 <= 1024 * 1000) {
            myContentLength2 /= 1024F;
            myDownloaded /= 1024F;
            myDownloaded = onlyTwoAfterComma(myDownloaded);
            myContentLength2 = onlyTwoAfterComma(myContentLength2);
            lblMb.setText(myDownloaded + " / " + myContentLength2 + " MB");
        } else {
            myContentLength2 /= (1024F * 1000F);
            myDownloaded /= (1024F * 1000F);
            myDownloaded = onlyTwoAfterComma(myDownloaded);
            myContentLength2 = onlyTwoAfterComma(myContentLength2);
            lblMb.setText(myDownloaded + " / " + myContentLength2 + " GB");
        }
    }

    @Override
    public void downloadComplete() {

        if (AUTOCLOSE_AFTER) {
            frmJmupdater.setVisible(false);
            System.exit(0);
        } else {
            progressBar.setIndeterminate(false);
            progressBar.setValue(100);

            lblDownloadingAndInstalling.setText("Download complete:");
            lblVersion.setText("Version " + UPDATE_TO + " installed.");

            progressBar.setForeground(Color.GREEN);
        }
    }

    public float onlyTwoAfterComma(float input) {

        return Math.round(input * 100F) / 100F;
    }

    @Override
    public void contentLengthAvialable(Long contentLength) {

        progressBar.setIndeterminate(false);
        progressBar.setMaximum(100);
        setContentLength(0L, contentLength);
    }

    @Override
    public void updateDownloadedLength(Long downloadedLength) {
        setContentLength(downloadedLength, myContentLength);
        progress = (int) (((float) downloadedLength / (float) myContentLength) * 100F);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setValue(progress);
            }
        });
    }
}