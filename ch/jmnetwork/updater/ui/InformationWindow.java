package ch.jmnetwork.updater.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class InformationWindow {

	private JFrame frmInfo;

	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// InformationWindow window = new InformationWindow();
	// window.frmInfo.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	public InformationWindow() {
		initialize();

		frmInfo.setVisible(true);
	}

	private void initialize() {
		frmInfo = new JFrame();
		frmInfo.setTitle("Info!");
		frmInfo.setBounds(100, 100, 959, 95);
		frmInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmInfo.getContentPane().setLayout(null);
		frmInfo.setLocationRelativeTo(null);

		JLabel lblUsageJmupdaterjar = new JLabel("USAGE: JMUpdater.jar {PROJECT_URL (VersionAPI)} {INSTALLED_VERSION} {UPDATE_TO_VERSION (Version or NEWEST)} {DIRECTORY_TO_INSTALL} {AUTOCLOSE_AFTER_UPDATE}");
		lblUsageJmupdaterjar.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 12));
		lblUsageJmupdaterjar.setBounds(10, 11, 916, 14);
		frmInfo.getContentPane().add(lblUsageJmupdaterjar);

		JProgressBar progressBar = new JProgressBar();
		progressBar.setValue(100);
		progressBar.setForeground(Color.RED);
		progressBar.setBounds(10, 36, 916, 5);
		frmInfo.getContentPane().add(progressBar);
	}

}
